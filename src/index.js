import React, { Component } from 'react';
import AppNavigator from './routes';

export default class App extends Component {
  render() {
    return (
      <AppNavigator/>
    );
  }
}