import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';
import LoginScreen from './pages/Login/index';

const App = createStackNavigator({
  Home: { screen: LoginScreen },
});

const AppNavigator = createAppContainer(App);
export default AppNavigator;